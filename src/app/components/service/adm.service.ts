import { DadosAdm } from './dados-adm';
import { MatSnackBar} from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuarios } from './usuarios';
import 'rxjs/add/operator/do'
import { delay, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdmService {

  constructor(private snackBar: MatSnackBar, private http: HttpClient) { }

  showMessage(msg: string): void{

    this.snackBar.open(msg, 'X',{
    duration: 3000,
    horizontalPosition: "right",
    verticalPosition: "top"
       
   })
    
}
    


  private url = "http://localhost:3000/users";
  private url2 = "http://localhost:3000/adm/login";
  private url3 = "http://localhost:3000/adm/cadastro";
  private url4 = "http://localhost:3000/adm";

  public check(): boolean{

    return localStorage.getItem('user') ? true : false;

  }

  public getInformationUser(): Observable<any> {

    return this.http.get<Usuarios[]>(this.url, {headers:{

      'Authorization': localStorage.getItem('Authorization')
    }}

    )
    .pipe(

      delay(2000)

    );

  };

  public getAmd(): Observable<any>{

    return this.http.get<DadosAdm[]>(this.url4, {headers:{

      'Authorization': localStorage.getItem('Authorization')

    }})

  }

  getadmDados(): DadosAdm{

    return localStorage.getItem('user') ? JSON.parse(atob(localStorage.getItem('user'))) : null;

  }

  public getAdmId(id: string): Observable<any>{

    const url = `${this.url4}/${id}`;
    return this.http.get<DadosAdm>(url, {headers:{

      'Authorization': localStorage.getItem('Authorization')

    }});


  }

  public getUserId(id_usuario: string): Observable<Usuarios>{

    const url = `${this.url}/${id_usuario}`;
    return this.http.get<Usuarios>(url, {headers:{

      'Authorization': localStorage.getItem('Authorization')

    }});


  }


  public login(any: any): Observable<boolean> {

    return this.http.post<any>(this.url2, any).do(data =>{

      localStorage.setItem('Authorization', `Bearer ${data.token}`)
    localStorage.setItem('user', btoa(JSON.stringify(data.Usuario)))

    })
    
   
  };

  public postAdm(adm: DadosAdm): Observable<DadosAdm[]> {

    return this.http.post<DadosAdm[]>(this.url3, adm, {headers:{

      'Authorization': localStorage.getItem('Authorization')

    }})

  };

  public editUser(user: Usuarios): Observable<Usuarios>{

    const url = `${this.url}/${user.id_usuario}`;
    return this.http.patch<Usuarios>(url,user, {headers:{

      'Authorization': localStorage.getItem('Authorization')


    }});

  };



  public deleteUser(id_usuario: number): Observable<Usuarios>{

    const url = `${this.url}/${id_usuario}`;
    return this.http.delete<Usuarios>(url);


  }


}




