import { delay } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AdmService } from './../service/adm.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuarios } from '../service/usuarios';



@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {


  usuario: any
  displayedColumns: string[] = ['nome', 'cpf', 'data_nascimento', 'actions'];
 

  constructor(private service: AdmService, private route: Router) {


  }

  loading = true

  ngOnInit() {


    this.service.getInformationUser().subscribe(
      
      (usuarios) => {
      this.usuario = usuarios;
        this.loading= false
      console.log(usuarios) 

    },

    (error)=>{

      this.service.showMessage('Faça primeiro o login'),
      this.loading = true
     this.route.navigate([''])

    }


    
    
    )

  
  }



}
