import { DadosAdm } from './../service/dados-adm';
import { ActivatedRoute, Router } from '@angular/router';
import { AdmService } from './../service/adm.service';
import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav/nav.service';


@Component({
  selector: 'app-adm-perfil',
  templateUrl: './adm-perfil.component.html',
  styleUrls: ['./adm-perfil.component.css']
})
export class AdmPerfilComponent implements OnInit {

  constructor(public service: AdmService, private activated: ActivatedRoute,private navService: NavService) {
      
    navService.navData = {
      icon: 'account_circle',
      router: '/perfil/adms'

    }

   }
 

  ngOnInit(): void {
    
  }
}
