import { AdmService } from './../service/adm.service';
import { AdmPerfilComponent } from './../adm-perfil/adm-perfil.component';
import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav/nav.service';



@Component({
  selector: 'app-pag-adm',
  templateUrl: './pag-adm.component.html',
  styleUrls: ['./pag-adm.component.css']
})
export class PagAdmComponent implements OnInit {


  constructor(public service: AdmService, private navService: NavService) {


    navService.navData = {
      icon: 'account_circle',
      router: '/perfil/adms'

    }


   }



  ngOnInit(): void {

  
}


}
