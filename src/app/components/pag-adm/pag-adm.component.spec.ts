import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagAdmComponent } from './pag-adm.component';

describe('PagAdmComponent', () => {
  let component: PagAdmComponent;
  let fixture: ComponentFixture<PagAdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagAdmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagAdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
