import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { DadosAdm } from './../service/dados-adm';
import { AdmService } from './../service/adm.service';
import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav/nav.service';



export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-cadastro-adm',
  templateUrl: './cadastro-adm.component.html',
  styleUrls: ['./cadastro-adm.component.css']
})
export class CadastroAdmComponent implements OnInit {

  adm:  DadosAdm= {

    id: '',
    nome: '',
    email: '',
    senha: ''


  }

  constructor(private service: AdmService, private route: Router, private navService: NavService) {

    
    navService.navData = {
      icon: 'account_circle',
      router: '/perfil/adms'

    }
    
   }

  ngOnInit(): void {
  }

  public cadastro(): void {

    this.service.postAdm(this.adm).subscribe(

      (data) => {

        this.service.showMessage('Administrador cadastrado')
        this.route.navigate(['/perfil/adms'])


      },

      (error) => {

        this.service.showMessage('Erro no cadastro')

      }


    )


  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();


  hide = true;



}
