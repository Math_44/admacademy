import { DadosAdm } from './../service/dados-adm';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { AdmService } from '../service/adm.service';



export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  adm: DadosAdm ={

    id: '',
    nome: '',
    email: '',
    senha: ''

  }



  constructor(private service: AdmService, private route: Router, private actvated: ActivatedRoute) { }

  ngOnInit(): void {

  }

  login(): void {

    this.service.login(this.adm).subscribe(
    
      (data)=>{

      console.log(data)
      this.service.showMessage('Login realizado')
      this.route.navigate(['pagadm'])

    },
    
      (error)=>{

        console.log(error)
        return this.service.showMessage('Email ou senha incorretos !')


      }


    )


  }


  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();


 hide = true;
  


}
