import { Usuarios } from './../service/usuarios';
import { AdmService } from './../service/adm.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavService } from '../nav/nav.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  usuarios: Usuarios

  constructor(private service: AdmService, private activated: ActivatedRoute, private route: Router,
    private navService: NavService) { 

      
    navService.navData = {
      icon: 'account_circle',
      router: '/perfil/adms'

    }

    }

  ngOnInit(): void {

    const id_usuario = this.activated.snapshot.paramMap.get('id_usuario')
    this.service.getUserId(id_usuario).subscribe(

      (usuario) => {

        this.usuarios = usuario
        console.log(usuario)

      })

  }


  public deleteUser(): void {
    
    
  this.service.deleteUser(this.usuarios.id_usuario).subscribe(

      (data) => {

        this.service.showMessage('Aluno excluido !')
        this.route.navigate(['/pagadm'])


      },

      (error) => {

          console.log(error)
        return this.service.showMessage('Erro tente novamente !')


      }


    )


  }


}
