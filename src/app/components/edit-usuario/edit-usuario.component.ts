import { Usuarios } from './../service/usuarios';
import { AdmService } from './../service/adm.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { NavService } from '../nav/nav.service';

@Component({
  selector: 'app-edit-usuario',
  templateUrl: './edit-usuario.component.html',
  styleUrls: ['./edit-usuario.component.css']
})
export class EditUsuarioComponent implements OnInit {

  usuarios: any;

  constructor(private service: AdmService, private activated: ActivatedRoute, private route: Router,
    private navService: NavService) {

      
    navService.navData = {
      icon: 'account_circle',
      router: '/perfil/adms'

    }
    
     }



  ngOnInit(): void {

    const id = this.activated.snapshot.paramMap.get('id_usuario')

    this.service.getUserId(id).subscribe((usuario) => {

      this.usuarios = usuario

      console.log(usuario)


    })

  }


  public editUsuario(): void {

    this.service.editUser(this.usuarios).subscribe(
      
      (data) => {

        this.service.showMessage("Dados atualizados !")
        this.route.navigate(["/pagadm"])
     },

     (error)=>{

      console.log(error)
      return this.service.showMessage('Dados Invalidos!')


    }
     
  )

     

}




}
