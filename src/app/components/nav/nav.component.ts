import { ActivatedRoute, Router } from '@angular/router';
import { AdmService } from './../service/adm.service';
import { DadosAdm } from './../service/dados-adm';
import { Component, OnInit } from '@angular/core';
import { NavService } from './nav.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  

  constructor(private navService: NavService, public service: AdmService) { }
 
  ngOnInit(): void {

  }

  get icon():string{

    return this.navService.navData.icon

  }

  get router():string{

    return this.navService.navData.router

  }

}
