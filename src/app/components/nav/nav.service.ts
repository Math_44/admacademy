import { NavData } from './nav.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  private nav = new BehaviorSubject<NavData>({

    icon: 'highlight_off',
    router: ''


  })

  constructor() { }

  get navData(): NavData{

    return this.nav.value

  }

  set navData(navData : NavData){

    this.nav.next(navData)

  }
}
