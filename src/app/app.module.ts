import { AdmService } from './components/service/adm.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './components/nav/nav.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { HeaderComponent } from './components/header/header.component';
import {MatInputModule} from '@angular/material/input';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { PagAdmComponent } from './components/pag-adm/pag-adm.component'
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import { TableComponent } from './components/table/table.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { EditUsuarioComponent } from './components/edit-usuario/edit-usuario.component';
import { FooterComponent } from './components/footer/footer.component';
import { DeleteUserComponent } from './components/delete-user/delete-user.component';
import { AdmPerfilComponent } from './components/adm-perfil/adm-perfil.component';
import { CadastroAdmComponent } from './components/cadastro-adm/cadastro-adm.component';
import {MatDividerModule} from '@angular/material/divider';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    PagAdmComponent,
    TableComponent,
    EditUsuarioComponent,
    FooterComponent,
    DeleteUserComponent,
    AdmPerfilComponent,
    CadastroAdmComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule, 
    MatIconModule,
    MatButtonModule, 
    MatInputModule,
    FormsModule,
    ReactiveFormsModule, 
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
   HttpClientModule, 
   MatTableModule,
   MatPaginatorModule,
   MatSortModule,
   MatDividerModule
  
    
    
  ],
  providers: [AdmService],
  bootstrap: [AppComponent]
})
export class AppModule { }
