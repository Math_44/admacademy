import { CadastroAdmComponent } from './components/cadastro-adm/cadastro-adm.component';
import { AdmPerfilComponent } from './components/adm-perfil/adm-perfil.component';
import { DeleteUserComponent } from './components/delete-user/delete-user.component';
import { EditUsuarioComponent } from './components/edit-usuario/edit-usuario.component';
import { PagAdmComponent } from './components/pag-adm/pag-adm.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';



const routes: Routes = [


  {
    path: "",
    component: LoginComponent

  },


  {

    path: "home",
    component: HomeComponent


  },

  {

    path: "pagadm",
    component: PagAdmComponent


  },

  {

    path: "usuario/edit/:id_usuario",
    component: EditUsuarioComponent

  }, 

  {

    path: "usuario/delete/:id_usuario",
    component: DeleteUserComponent

  },

  {

      path:"perfil/adms",
      component: AdmPerfilComponent

  },

  {

    path:"cadastro/adm",
    component: CadastroAdmComponent

  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
